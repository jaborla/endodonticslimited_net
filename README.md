# EndodonticsLimited.net
## Purpose
This repo builds a Droplet in DigitalOcean and deploys the included website to it

## Requirements
- `ansible` installed
- SSH keys in `~/.ssh/digitalocean_rsa.pub` and `~/.ssh/onyx_rsa.pub`

## One-button
- `./execute.sh`

## Setup
* `ansible-galaxy install -r requirements/do.yaml`

## Execution
* `ansible-playbook -i inventories/hosts playbook.yaml --ask-vault-pass` 
