#!/usr/bin/env bash
ansible-galaxy install -r requirements/do.yaml
ansible-playbook -i inventories/hosts playbook.yaml --ask-vault-pass
